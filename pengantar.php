<?php

require_once "functions/DB_Pengantar.php";
$database = new DB_Pengantar();
require_once "functions/My_Date.php";
$my_date = new My_Date();

$response = array("error" => false);

if (isset($_POST["parInput"])) {
	$par_input = $_POST["parInput"];
	$par_input_array = explode("#", $par_input);
	$id_login = $par_input_array[0];
	$password = $par_input_array[1];
	$imei = $par_input_array[2];
	$versi = $par_input_array[3];
	$server = $par_input_array[4];
	$encrypted_password = md5($id_login.$password);
	$new_par_input = $id_login."#".$encrypted_password."#".$imei;
	$tanggal_perangkat = $par_input_array[5];
	$tanggal_server = date("Y-m-d");

	if (count($par_input_array) > 6) {
		$response["error"] = true;
		$response["error_code"] = "999";
		$response["error_message"] ="Password tidak boleh menggunakan tanda baca pagar (#)";
		echo json_encode($response);
	} else {
		if ($tanggal_server != $tanggal_perangkat) {
			$response["error"] = true;
			$response["error_code"] = "999";
			$response["error_message"] =
				"Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server
				\nTanggal Perangkat : " . $my_date->convert_to_date_indo($tanggal_perangkat) . "\nTanggal Server       : " . $my_date->convert_to_date_indo($tanggal_server);
			echo json_encode($response);
		} else {
			ini_set("soap.wsdl_cache_enabled", 0);
			ini_set("soap.wsdl_cache_ttl", 0);
			try {
				//$wsdl = "http://10.33.41.60:39763/services/DATA_MOBILE?wsdl";
				//$wsdl = "http://10.33.41.76:9763/services/DATA_MOBILE?wsdl";
				$wsdl = "DATA_MOBILE.xml";

				$client = new SoapClient($wsdl, array("cache_wsdl" => WSDL_CACHE_NONE) );
				$store_data = $client->OGetPetugas (array("ParInput" => $new_par_input));
				$get_response = $store_data;
				$get_response = $store_data->RESPON;
				$iposweb_response = $get_response->RESPON;
				$iposweb_response_array = explode("|", $iposweb_response);

				if ($iposweb_response_array[0] <> "000") {
					$response["error"] = true;
					$response["error_code"] = $iposweb_response_array[0];
					$response["error_message"] = $iposweb_response_array[1];
					echo json_encode($response);
				} else {
					$id = $iposweb_response_array[2];
					$nama = strtoupper($iposweb_response_array[3]);
					$id_login = $iposweb_response_array[1];
					$id_kantor = $iposweb_response_array[6];
					$id_grup = $iposweb_response_array[9];
					$id_mandor = $iposweb_response_array[15];
					$nama_mandor = $iposweb_response_array[16];

					if ($database->is_user_exist($id)) {
						$result = $database->update($id, $nama, $id_login, $password, $imei, $id_kantor, $id_grup, $id_mandor, $nama_mandor, $versi, $server);
					} else {
						$result = $database->insert($id, $nama, $id_login, $password, $imei, $id_kantor, $id_grup, $id_mandor, $nama_mandor, $versi, $server);
					}

					if ($result) {
						$response["error"] = false;
						$response["pengantar"]["id"] = $id;
						$response["pengantar"]["nama"] = $nama;
						$response["pengantar"]["id_login"] = $id_login;
						$response["pengantar"]["id_kantor"] = $id_kantor;
						$response["pengantar"]["id_grup"] = $id_grup;
						$response["pengantar"]["id_mandor"] = $id_mandor;
						$response["pengantar"]["nama_mandor"] = $nama_mandor;
						echo json_encode($response);
					} else {
						$response["error"] = true;
						$response["error_message"] = "Terjadi kesalahan pada saat menjalankan perintah. Silakan dicoba kembali";
						echo json_encode($response);
					}
				}
			} catch (Exception $e) {
				echo $e->getiposweb_detail();
			}
		}
	}
}

?>
