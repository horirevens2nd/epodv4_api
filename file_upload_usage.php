<?php
require_once "functions/DB_File_Upload_Usage.php";
$database = new DB_File_Upload_Usage();
require_once "functions/My_Date.php";
$my_date = new My_Date();

$response = array("error" => false);

if (isset($_POST["action"]) && $_POST["action"] == "submit") {
    $uid = $_POST["uid"];
    $id_pengantar = $_POST["id_pengantar"];
    $tanggal = $_POST["tanggal"];
    $jml_berkas = $_POST["jml_berkas"];
    $jml_berkas_foto = $_POST["jml_berkas_foto"];
    $jml_berkas_ttd = $_POST["jml_berkas_ttd"];
    $jml_ukuran = $_POST["jml_ukuran"];
    $jml_ukuran_foto = $_POST["jml_ukuran_foto"];
    $jml_ukuran_ttd = $_POST["jml_ukuran_ttd"];
    $status_update = $_POST["status_update"];

    if ($database->is_empty($tanggal, $id_pengantar)) {
        $result = $database->insert(
            $uid, $id_pengantar, $tanggal, $jml_berkas, $jml_berkas_foto, $jml_berkas_ttd, $jml_ukuran, $jml_ukuran_foto,
            $jml_ukuran_ttd
        );

        if ($result) {
            $response["error"] = false;
            $response["file_upload_usage"]["action"] = "insert";
            $response["file_upload_usage"]["uid"] = $result["uid"];
            $response["file_upload_usage"]["tanggal"] = $my_date->convert_to_date($result["tanggal"]);
            $response["file_upload_usage"]["jml_berkas"] = $result["jml_berkas"];
            $response["file_upload_usage"]["jml_berkas_foto"] = $result["jml_berkas_foto"];
            $response["file_upload_usage"]["jml_berkas_ttd"] = $result["jml_berkas_ttd"];
            $response["file_upload_usage"]["jml_ukuran"] = $result["jml_ukuran"];
            $response["file_upload_usage"]["jml_ukuran_foto"] = $result["jml_ukuran_foto"];
            $response["file_upload_usage"]["jml_ukuran_ttd"] = $result["jml_ukuran_ttd"];
            echo json_encode($response);
        } else {
            $response["error"] = true;
            $response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
            echo json_encode($response);
        }
    } else {
        $latest_data = $database->get_latest_data($tanggal, $id_pengantar);

        if ($latest_data) {
            $uid_latest = $latest_data["uid"];
            $jml_berkas_latest = $latest_data["jml_berkas"];
            $jml_berkas_foto_latest = $latest_data["jml_berkas_foto"];
            $jml_berkas_ttd_latest = $latest_data["jml_berkas_ttd"];
            $jml_ukuran_latest = $latest_data["jml_ukuran"];
            $jml_ukuran_foto_latest = $latest_data["jml_ukuran_foto"];
            $jml_ukuran_ttd_latest = $latest_data["jml_ukuran_ttd"];

            if ($status_update == "1") {
                $jml_berkas_newest = $jml_berkas_latest + $jml_berkas;
                $jml_berkas_foto_newest = $jml_berkas_foto_latest + $jml_berkas_foto;
                $jml_berkas_ttd_newest = $jml_berkas_ttd_latest + $jml_berkas_ttd;
                $jml_ukuran_newest = $jml_ukuran_latest + $jml_ukuran;
                $jml_ukuran_foto_newest = $jml_ukuran_foto_latest + $jml_ukuran_foto;
                $jml_ukuran_ttd_newest = $jml_ukuran_ttd_latest + $jml_ukuran_ttd;
            
                if ($database->update(
                    $uid_latest, $jml_berkas_newest, $jml_berkas_foto_newest, $jml_berkas_ttd_newest, $jml_ukuran_newest,
                    $jml_ukuran_foto_newest, $jml_ukuran_ttd_newest)
                ) {
                    $response["error"] = false;
                    $response["file_upload_usage"]["action"] = "update";
                    $response["file_upload_usage"]["uid"] = $uid_latest;
                    $response["file_upload_usage"]["tanggal"] = $tanggal;
                    $response["file_upload_usage"]["jml_berkas"] = $jml_berkas_newest;
                    $response["file_upload_usage"]["jml_berkas_foto"] = $jml_berkas_foto_newest;
                    $response["file_upload_usage"]["jml_berkas_ttd"] = $jml_berkas_ttd_newest;
                    $response["file_upload_usage"]["jml_ukuran"] = $jml_ukuran_newest;
                    $response["file_upload_usage"]["jml_ukuran_foto"] = $jml_ukuran_foto_newest;
                    $response["file_upload_usage"]["jml_ukuran_ttd"] = $jml_ukuran_ttd_newest;
                    echo json_encode($response);
                } else {
                    $response["error"] = true;
                    $response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
                    echo json_encode($response);
                }
            } else {
                if ($database->update(
                    $uid_latest, $jml_berkas, $jml_berkas_foto, $jml_berkas_ttd, $jml_ukuran, $jml_ukuran_foto, $jml_ukuran_ttd)
                ) {
                    $response["error"] = false;
                    $response["file_upload_usage"]["action"] = "update";
                    $response["file_upload_usage"]["uid"] = $uid_latest;
                    $response["file_upload_usage"]["tanggal"] = $tanggal;
                    $response["file_upload_usage"]["jml_berkas"] = $jml_berkas;
                    $response["file_upload_usage"]["jml_berkas_foto"] = $jml_berkas_foto;
                    $response["file_upload_usage"]["jml_berkas_ttd"] = $jml_berkas_ttd;
                    $response["file_upload_usage"]["jml_ukuran"] = $jml_ukuran;
                    $response["file_upload_usage"]["jml_ukuran_foto"] = $jml_ukuran_foto;
                    $response["file_upload_usage"]["jml_ukuran_ttd"] = $jml_ukuran_ttd;
                    echo json_encode($response);
                } else {
                    $response["error"] = true;
                    $response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
                    echo json_encode($response);
                }
            }
        }
    }
}

if (isset($_POST["action"]) && $_POST["action"] == "get_all") {
    $id_pengantar = $_POST["id_pengantar"];
    $result = $database->get_all($id_pengantar);

    if ($result == "empty") {
        $response["error"] = false;
        $response["message"] = "empty";
        echo json_encode($response);
    } else if ($result == false) {
        $response["error"] = true;
        $response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
        echo json_encode($response);
    } else {
        $length = count($result);
        $response["error"] = false;
        $response["message"] = "not_empty";

        for ($i=0; $i<$length; $i++) {
            $response["file_upload_usage"][$i]["uid"] = $result[$i]["uid"];
            $response["file_upload_usage"][$i]["tanggal"] = $my_date->convert_to_date($result[$i]["tanggal"]);
            $response["file_upload_usage"][$i]["jml_berkas"] = $result[$i]["jml_berkas"];
            $response["file_upload_usage"][$i]["jml_berkas_foto"] = $result[$i]["jml_berkas_foto"];
            $response["file_upload_usage"][$i]["jml_berkas_ttd"] = $result[$i]["jml_berkas_ttd"];
            $response["file_upload_usage"][$i]["jml_ukuran"] = $result[$i]["jml_ukuran"];
            $response["file_upload_usage"][$i]["jml_ukuran_foto"] = $result[$i]["jml_ukuran_foto"];
            $response["file_upload_usage"][$i]["jml_ukuran_ttd"] = $result[$i]["jml_ukuran_ttd"];
        }
        echo json_encode($response);
    }
}

?>
