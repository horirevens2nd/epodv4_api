<?php

require_once "functions/DB_Antaran.php";
$database = new DB_Antaran();
require_once "functions/My_Date.php";
$my_date = new My_Date();
require_once "functions/API_Cash_On_Delivery.php";
$api_cod = new API_Cash_On_Delivery();

$response = array("error" => false);

if (isset($_POST["action"]) && $_POST["action"] == "insert") {
    $uid = uniqid("", true);
    $id_item = $_POST["id_item"];
    $id_delivery_order = $_POST["id_delivery_order"];
    $waktu_entri = $_POST["waktu_entri"];
    $id_mandor = $_POST["id_mandor"];
    $id_pengantar = $_POST["id_pengantar"];
    $id_kantor = $_POST["id_kantor"];
    $tanggal_perangkat = $_POST["tanggal_perangkat"];
    $tanggal_server = date("Y-m-d");
    $token = $api_cod->get_token();

    if ($token) {
        $items = $api_cod->get_item($id_item);
        $respcode = $items["response"]["respcode"];
        $barcode = $items["response"]["data"]["barcode"];

        if ($respcode == "000" || $respcode == "999") {
            if ($respcode == "000") {
                $nama_pengirim = str_replace("'", "", $items["response"]["data"]["shipper"]["name"]);
                $alamat_pengirim = $items["response"]["data"]["shipper"]["address"];
                $hp_pengirim = $items["response"]["data"]["shipper"]["hp"];
                $nama_penerima = str_replace("'", "", $items["response"]["data"]["receiver"]["name"]);
                $alamat_penerima = $items["response"]["data"]["receiver"]["address"];
                $hp_penerima = $items["response"]["data"]["receiver"]["hp"];
                $id_produk = $items["response"]["data"]["item"]["productId"];
                $berat = $items["response"]["data"]["item"]["weight"] != "" ? $items["response"]["data"]["item"]["weight"] : "0";
                $bsu_cod = $items["response"]["data"]["item"]["codValue"] != "" ? $items["response"]["data"]["item"]["codValue"] : "0";
                $bsu_blb = $items["response"]["data"]["item"]["blbValue"] != "" ? $items["response"]["data"]["item"]["blbValue"] : "0";
                $bsu_tax = $items["response"]["data"]["item"]["taxValue"] != "" ? $items["response"]["data"]["item"]["taxValue"] : "0";
                $kantor_asal = $items["response"]["data"]["detail"]["originOffice"];
                $kantor_tujuan = $items["response"]["data"]["detail"]["destOffice"];
                $id_customer = $items["response"]["data"]["detail"]["customerId"];
                $id_external = $items["response"]["data"]["detail"]["extId"];
                $tgl_posting = $items["response"]["data"]["detail"]["eventDate"];
                $tipe = $items["response"]["data"]["detail"]["note"];
            } elseif ($respcode == "999") {
                $nama_pengirim = "NOT FOUND";
                $alamat_pengirim = "NOT FOUND";
                $hp_pengirim = "";
                $nama_penerima = "NOT FOUND";
                $alamat_penerima = "NOT FOUND";
                $hp_penerima = "";
                $id_produk = "000";
                $berat = "0";
                $bsu_cod = "0";
                $bsu_blb = "0";
                $bsu_tax = "0";
                $kantor_asal = "";
                $kantor_tujuan = "";
                $id_customer = "";
                $id_external = "";
                $tgl_posting = $tanggal_perangkat;
                $tipe = "";
            }

            if ($tanggal_server == $tanggal_perangkat) {
                if ($database->is_id_item_exist($id_item)) {
                    $id_status = $database->get_id_status($id_item);

                    if ($id_status == "P01") { // Inproses
                        $nama = strtoupper($database->get_nama_pengantar($id_item));
                        $response["error"] = true;
                        $response["error_type"] = "toast";
                        $response["error_message"] = "Item sudah di-delivery order oleh " . $nama;
                        echo json_encode($response);
                    } else if ( // Berhasil antar
                        $id_status == "B01" || $id_status == "B02" || $id_status == "B03" || $id_status == "B04" | $id_status == "B05" || $id_status == "B06" ||
                        $id_status == "B07" || $id_status == "B08" || $id_status == "B09" || $id_status == "B10" | $id_status == "B11" || $id_status == "B12"
                    ) {
                        $nama = strtoupper($database->get_nama_pengantar($id_item));
                        $response["error"] = true;
                        $response["error_type"] = "toast";
                        $response["error_message"] = "Item sudah di-update status berhasil oleh " . $nama;
                        echo json_encode($response);
                    } else if (
                        $id_status == "G0501" || $id_status == "G0502" || $id_status == "G0503" || $id_status == "G0504" || $id_status == "G0505" ||
                        $id_status == "G0506" || $id_status == "G0507"
                    ) { // Antar ulang
                        if ($database->is_less_than_three_times($id_item)) {
                            $result = $database->insert(
                                $id_item, $id_delivery_order, $waktu_entri, $id_mandor, $id_pengantar, $id_kantor, $nama_pengirim, $alamat_pengirim, $hp_pengirim, $nama_penerima, $alamat_penerima, $hp_penerima, $id_produk, $berat,
                                $bsu_cod, $bsu_blb, $bsu_tax, $kantor_asal, $kantor_tujuan, $id_customer, $id_external, $tgl_posting, $tipe
                            );

                            if ($result) {
                                $response["error"] = false;
                                $response["antaran"]["uid"] = $result["uid"];
                                $response["antaran"]["id_item"] = $result["id_item"];
                                $response["antaran"]["id_delivery_order"] = $result["id_delivery_order"];
                                $response["antaran"]["id_status"] = $result["id_status"];
                                $response["antaran"]["keterangan"] = $result["keterangan"];
                                $response["antaran"]["waktu_entri"] = $my_date->convert_to_datetime($result["waktu_entri"]);
                                $response["antaran"]["waktu_update"] = $result["waktu_update"];
                                $response["antaran"]["garis_lintang"] = $result["garis_lintang"];
                                $response["antaran"]["garis_bujur"] = $result["garis_bujur"];
                                $response["antaran"]["tanda_tangan"] = $result["tanda_tangan"];
                                $response["antaran"]["foto"] = $result["foto"];
                                $response["antaran"]["nama_pengirim"] = $result["nama_pengirim"];
                                $response["antaran"]["alamat_pengirim"] = $result["alamat_pengirim"];
                                $response["antaran"]["hp_pengirim"] = $result["hp_pengirim"];
                                $response["antaran"]["nama_penerima"] = $result["nama_penerima"];
                                $response["antaran"]["alamat_penerima"] = $result["alamat_penerima"];
                                $response["antaran"]["hp_penerima"] = $result["hp_penerima"];
                                $response["antaran"]["id_produk"] = $result["id_produk"];
                                $response["antaran"]["berat"] = $result["berat"];
                                $response["antaran"]["bsu_cod"] = $result["bsu_cod"];
                                $response["antaran"]["bsu_blb"] = $result["bsu_blb"];
                                $response["antaran"]["bsu_tax"] = $result["bsu_tax"];
                                $response["antaran"]["kantor_asal"] = $result["kantor_asal"];
                                $response["antaran"]["kantor_tujuan"] = $result["kantor_tujuan"];
                                $response["antaran"]["id_customer"] = $result["id_customer"];
                                $response["antaran"]["id_external"] = $result["id_external"];
                                $response["antaran"]["tgl_posting"] = $result["tgl_posting"];
                                $response["antaran"]["tipe"] = $result["tipe"];
                                $response["antaran"]["alamat_baru"] = $result["alamat_baru"];
                                $response["antaran"]["catatan"] = $result["catatan"];
                                echo json_encode($response);
                            } else {
                                $response["error"] = true;
                                $response["error_type"] = "toast";
                                $response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
                                echo json_encode($response);
                            }
                        } else {
                            $response["error"] = true;
                            $response["error_type"] = "toast";
                            $response["error_message"] = "Item sudah 3x antaran dan harus segera dilakukan retur";
                            echo json_encode($response);
                        }
                    } else { // Retur/Dipanggil/Ditahan/Diteruskan
                        $result = $database->insert(
                            $id_item, $id_delivery_order, $waktu_entri, $id_mandor, $id_pengantar, $id_kantor, $nama_pengirim, $alamat_pengirim, $hp_pengirim, $nama_penerima, $alamat_penerima, $hp_penerima, $id_produk, $berat,
                            $bsu_cod, $bsu_blb, $bsu_tax, $kantor_asal, $kantor_tujuan, $id_customer, $id_external, $tgl_posting, $tipe
                        );

                        if ($result) {
                            $response["error"] = false;
                            $response["antaran"]["uid"] = $result["uid"];
                            $response["antaran"]["id_item"] = $result["id_item"];
                            $response["antaran"]["id_delivery_order"] = $result["id_delivery_order"];
                            $response["antaran"]["id_status"] = $result["id_status"];
                            $response["antaran"]["keterangan"] = $result["keterangan"];
                            $response["antaran"]["waktu_entri"] = $my_date->convert_to_datetime($result["waktu_entri"]);
                            $response["antaran"]["waktu_update"] = $result["waktu_update"];
                            $response["antaran"]["garis_lintang"] = $result["garis_lintang"];
                            $response["antaran"]["garis_bujur"] = $result["garis_bujur"];
                            $response["antaran"]["tanda_tangan"] = $result["tanda_tangan"];
                            $response["antaran"]["foto"] = $result["foto"];
                            $response["antaran"]["nama_pengirim"] = $result["nama_pengirim"];
                            $response["antaran"]["alamat_pengirim"] = $result["alamat_pengirim"];
                            $response["antaran"]["hp_pengirim"] = $result["hp_pengirim"];
                            $response["antaran"]["nama_penerima"] = $result["nama_penerima"];
                            $response["antaran"]["alamat_penerima"] = $result["alamat_penerima"];
                            $response["antaran"]["hp_penerima"] = $result["hp_penerima"];
                            $response["antaran"]["id_produk"] = $result["id_produk"];
                            $response["antaran"]["berat"] = $result["berat"];
                            $response["antaran"]["bsu_cod"] = $result["bsu_cod"];
                            $response["antaran"]["bsu_blb"] = $result["bsu_blb"];
                            $response["antaran"]["bsu_tax"] = $result["bsu_tax"];
                            $response["antaran"]["kantor_asal"] = $result["kantor_asal"];
                            $response["antaran"]["kantor_tujuan"] = $result["kantor_tujuan"];
                            $response["antaran"]["id_customer"] = $result["id_customer"];
                            $response["antaran"]["id_external"] = $result["id_external"];
                            $response["antaran"]["tgl_posting"] = $result["tgl_posting"];
                            $response["antaran"]["tipe"] = $result["tipe"];
                            $response["antaran"]["alamat_baru"] = $result["alamat_baru"];
                            $response[" ntaran"]["catatan"] = $result["catatan"];
                            echo json_encode($response);
                        } else {
                            $response["error"] = true;
                            $response["error_type"] = "toast";
                            $response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
                            echo json_encode($response);
                        }
                    }
                } else {
                    $result = $database->insert(
                        $id_item, $id_delivery_order, $waktu_entri, $id_mandor, $id_pengantar, $id_kantor, $nama_pengirim, $alamat_pengirim, $hp_pengirim, $nama_penerima, $alamat_penerima, $hp_penerima, $id_produk, $berat,
                        $bsu_cod, $bsu_blb, $bsu_tax, $kantor_asal, $kantor_tujuan, $id_customer, $id_external, $tgl_posting, $tipe
                    );

                    if ($result) {
                        $response["error"] = false;
                        $response["antaran"]["uid"] = $result["uid"];
                        $response["antaran"]["id_item"] = $result["id_item"];
                        $response["antaran"]["id_delivery_order"] = $result["id_delivery_order"];
                        $response["antaran"]["id_status"] = $result["id_status"];
                        $response["antaran"]["keterangan"] = $result["keterangan"];
                        $response["antaran"]["waktu_entri"] = $my_date->convert_to_datetime($result["waktu_entri"]);
                        $response["antaran"]["waktu_update"] = $result["waktu_update"];
                        $response["antaran"]["garis_lintang"] = $result["garis_lintang"];
                        $response["antaran"]["garis_bujur"] = $result["garis_bujur"];
                        $response["antaran"]["tanda_tangan"] = $result["tanda_tangan"];
                        $response["antaran"]["foto"] = $result["foto"];
                        $response["antaran"]["nama_pengirim"] = $result["nama_pengirim"];
                        $response["antaran"]["alamat_pengirim"] = $result["alamat_pengirim"];
                        $response["antaran"]["hp_pengirim"] = $result["hp_pengirim"];
                        $response["antaran"]["nama_penerima"] = $result["nama_penerima"];
                        $response["antaran"]["alamat_penerima"] = $result["alamat_penerima"];
                        $response["antaran"]["hp_penerima"] = $result["hp_penerima"];
                        $response["antaran"]["id_produk"] = $result["id_produk"];
                        $response["antaran"]["berat"] = $result["berat"];
                        $response["antaran"]["bsu_cod"] = $result["bsu_cod"];
                        $response["antaran"]["bsu_blb"] = $result["bsu_blb"];
                        $response["antaran"]["bsu_tax"] = $result["bsu_tax"];
                        $response["antaran"]["kantor_asal"] = $result["kantor_asal"];
                        $response["antaran"]["kantor_tujuan"] = $result["kantor_tujuan"];
                        $response["antaran"]["id_customer"] = $result["id_customer"];
                        $response["antaran"]["id_external"] = $result["id_external"];
                        $response["antaran"]["tgl_posting"] = $result["tgl_posting"];
                        $response["antaran"]["tipe"] = $result["tipe"];
                        $response["antaran"]["alamat_baru"] = $result["alamat_baru"];
                        $response["antaran"]["catatan"] = $result["catatan"];
                        echo json_encode($response);
                    } else {
                        $response["error"] = true;
                        $response["error_type"] = "toast";
                        $response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
                        echo json_encode($response);
                    }
                }
            } else {
                $response["error"] = true;
                $response["error_type"] = "alert";
                $response["error_message"] =
                "Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server
                \nTanggal Perangkat : " . $my_date->convert_to_date_indo($tanggal_perangkat) . "\nTanggal Server       : " . $my_date->convert_to_date_indo($tanggal_server);
                echo json_encode($response);
            }
        } else {
            if ($respcode == "998") {
                $response["error"] = true;
                $response["error_type"] = "alert";
                $response["error_message"] = "Kode otentikasi API salah. Mohon untuk menginformasikan ke pihak pengembang";
                echo json_encode($response);
            } elseif ($respcode == "997") {
                $response["error"] = true;
                $response["error_type"] = "toast";
                $response["error_message"] = "Data kiriman masih berstatus \"Add Posting\". Silakan dicoba kembali";
                echo json_encode($response);
            } else {
                $response["error"] = true;
                $response["error_type"] = "alert";
                $response["error_message"] = "Terjadi kesalahan tidak terdefinisi. Silakan dicoba kembali";
                echo json_encode($response);
            }
        }
    } else {
        $response["error"] = true;
        $response["error_type"] = "alert";
        $response["error_message"] = "Tidak dapat terhubung dengan API. Mohon untuk menginformasikan ke pihak pengembang";
        echo json_encode($response);
    }
}

if (isset($_POST["action"]) && $_POST["action"] == "delete_by_uid") {
	$uid = $_POST["uid"];

	if ($database->delete_by_uid($uid)) {
		$response["error"] = false;
		echo json_encode($response);
	} else {
		$response["error"] = true;
		$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
		echo json_encode($response);
	}
}

if (isset($_POST["action"]) && $_POST["action"] == "delete_by_id_delivery_order") {
	$id_delivery_order = $_POST["id_delivery_order"];

	if ($database->delete_by_id_delivery_order($id_delivery_order)) {
		$response["error"] = false;
		echo json_encode($response);
	} else {
		$response["error"] = true;
		$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
		echo json_encode($response);
	}
}

if (isset($_POST["action"]) && $_POST["action"] == "get_all") {
	require_once "functions/My_Date.php";
	$my_date = new My_Date();

	$id_pengantar = $_POST["id_pengantar"];
	$result = $database->get_all($id_pengantar);

	if ($result == "empty") {
		$response["error"] = false;
		$response["message"] = "empty";
		echo json_encode($response);
	} else if ($result == false) {
		$response["error"] = true;
		$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
		echo json_encode($response);
	} else {
		$length = count($result);
		$response["error"] = false;
		$response["message"] = "not_empty";

		for($i=0; $i<$length; $i++) {
			$response["antaran"][$i]["uid"] = $result[$i]["uid"];
			$response["antaran"][$i]["id_item"] = $result[$i]["id_item"];
			$response["antaran"][$i]["id_delivery_order"] = $result[$i]["id_delivery_order"];
			$response["antaran"][$i]["id_status"] = $result[$i]["id_status"];
			$response["antaran"][$i]["keterangan"] = $result[$i]["keterangan"];
			$response["antaran"][$i]["waktu_entri"] = $my_date->convert_to_datetime($result[$i]["waktu_entri"]);
			if ($result[$i]["waktu_update"] == null) {
				$response["antaran"][$i]["waktu_update"] = $result[$i]["waktu_update"];
			} else {
				$response["antaran"][$i]["waktu_update"] = $my_date->convert_to_datetime($result[$i]["waktu_update"]);
			}
			$response["antaran"][$i]["garis_lintang"] = $result[$i]["garis_lintang"];
			$response["antaran"][$i]["garis_bujur"] = $result[$i]["garis_bujur"];
			$response["antaran"][$i]["tanda_tangan"] = $result[$i]["tanda_tangan"];
			$response["antaran"][$i]["foto"] = $result[$i]["foto"];
			$response["antaran"][$i]["nama_pengirim"] = $result[$i]["nama_pengirim"];
			$response["antaran"][$i]["alamat_pengirim"] = $result[$i]["alamat_pengirim"];
			$response["antaran"][$i]["hp_pengirim"] = $result[$i]["hp_pengirim"];
			$response["antaran"][$i]["nama_penerima"] = $result[$i]["nama_penerima"];
			$response["antaran"][$i]["alamat_penerima"] = $result[$i]["alamat_penerima"];
			$response["antaran"][$i]["hp_penerima"] = $result[$i]["hp_penerima"];
			$response["antaran"][$i]["id_produk"] = $result[$i]["id_produk"];
			$response["antaran"][$i]["berat"] = $result[$i]["berat"];
			$response["antaran"][$i]["bsu_cod"] = $result[$i]["bsu_cod"];
			$response["antaran"][$i]["bsu_blb"] = $result[$i]["bsu_blb"];
			$response["antaran"][$i]["bsu_tax"] = $result[$i]["bsu_tax"];
			$response["antaran"][$i]["kantor_asal"] = $result[$i]["kantor_asal"];
			$response["antaran"][$i]["kantor_tujuan"] = $result[$i]["kantor_tujuan"];
			$response["antaran"][$i]["id_customer"] = $result[$i]["id_customer"];
			$response["antaran"][$i]["id_external"] = $result[$i]["id_external"];
			$response["antaran"][$i]["tgl_posting"] = $result[$i]["tgl_posting"];
            $response["antaran"][$i]["tipe"] = $result[$i]["tipe"];
            $response["antaran"][$i]["alamat_baru"] = $result[$i]["alamat_baru"];
			$response["antaran"][$i]["catatan"] = $result[$i]["catatan"];
			$response["antaran"][$i]["tutup"] = $result[$i]["tutup"];
		}
		echo json_encode($response);
	}
}

/* if (isset($_POST["action"]) && $_POST["action"] == "get_all_with_detail") {
	require_once "functions/My_Date.php";
	$my_date = new My_Date();

	$id_pengantar = $_POST["id_pengantar"];
	$result = $database->get_all_with_detail($id_pengantar);

	if ($result == "empty") {
		$response["error"] = false;
		$response["message"] = "empty";
		echo json_encode($response);
	} else if ($result == false) {
		$response["error"] = true;
		$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
		echo json_encode($response);
	} else {
		$length = count($result);
		$response["error"] = false;
		$response["message"] = "not_empty";

		for($i=0; $i<$length; $i++) {
			$response["antaran"][$i]["uid"] = $result[$i]["uid"];
			$response["antaran"][$i]["id_item"] = $result[$i]["id_item"];
			$response["antaran"][$i]["id_delivery_order"] = $result[$i]["id_delivery_order"];
			$response["antaran"][$i]["id_status"] = $result[$i]["id_status"];
			$response["antaran"][$i]["keterangan"] = $result[$i]["keterangan"];
			$response["antaran"][$i]["waktu_entri"] = $my_date->convert_to_datetime($result[$i]["waktu_entri"]);
			if ($result[$i]["waktu_update"] == null) {
				$response["antaran"][$i]["waktu_update"] = $result[$i]["waktu_update"];
			} else {
				$response["antaran"][$i]["waktu_update"] = $my_date->convert_to_datetime($result[$i]["waktu_update"]);
			}
			$response["antaran"][$i]["garis_lintang"] = $result[$i]["garis_lintang"];
			$response["antaran"][$i]["garis_bujur"] = $result[$i]["garis_bujur"];
			$response["antaran"][$i]["tanda_tangan"] = $result[$i]["tanda_tangan"];
			$response["antaran"][$i]["foto"] = $result[$i]["foto"];
			$response["antaran"][$i]["tutup"] = $result[$i]["tutup"];
			$response["antaran"][$i]["id_produk"] = $result[$i]["id_produk"];
			$response["antaran"][$i]["id_penerima"] = $result[$i]["id_penerima"];
			$response["antaran"][$i]["al_penerima"] = $result[$i]["al_penerima"];
			$response["antaran"][$i]["id_pengirim"] = $result[$i]["id_pengirim"];
			$response["antaran"][$i]["swp"] = $result[$i]["swp"];
		}
		echo json_encode($response);
	}
} */

if (isset($_POST["action"]) && $_POST["action"] == "update") {
	$uid = $_POST["uid"];
	$id_status = $_POST["id_status"];
	$keterangan = strtoupper($_POST["keterangan"]);
	$waktu_update = $_POST["waktu_update"];
	$garis_lintang = $_POST["garis_lintang"];
	$garis_bujur = $_POST["garis_bujur"];
	$tanda_tangan = $_POST["tanda_tangan"];
	$foto = $_POST["foto"];
	$id_item = $_POST["id_item"];
	$id_delivery_order = $_POST["id_delivery_order"];
	$id_pengantar = $_POST["id_pengantar"];
	$id_kantor = $_POST["id_kantor"];
	$kode_dn = $_POST["kode_dn"];
	$tanggal_perangkat = $_POST["tanggal_perangkat"];
	$tanggal_server = date("Y-m-d");
    $alamat_baru = $_POST["alamat_baru"];
    $catatan = $_POST["catatan"];

	if ($tanggal_server == $tanggal_perangkat) {
		if ($database->update(
			$uid, $id_status, $keterangan, $waktu_update, $garis_lintang, $garis_bujur, $tanda_tangan, $foto, $id_item,
			$id_delivery_order, $id_pengantar, $id_kantor, $kode_dn, $alamat_baru, $catatan)
		) {
			$response["error"] = false;
			echo json_encode($response);
		} else {
			$response["error"] = true;
			$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
			echo json_encode($response);
		}
	} else {
		$response["error"] = true;
		$response["error_type"] = "alert";
		$response["error_message"] =
			"Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server
			\nTanggal Perangkat : " . $my_date->convert_to_date_indo($tanggal_perangkat) . "\nTanggal Server       : " . $my_date->convert_to_date_indo($tanggal_server);
		echo json_encode($response);
	}
}

?>
