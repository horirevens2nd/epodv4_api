<?php

class DB_Connect {
	private $conn;

	public function connect() {
		require_once "database/Config.php";

		$this->conn = mssql_connect(DB_HOST, DB_USER, DB_PASSWORD);
		mssql_select_db(DB_DATABASE);

		return $this->conn;
	}
}

?>
