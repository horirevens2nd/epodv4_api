<?php

class DB_Delivery_Order {
	private $conn;

	function __construct() {
		require_once "database/DB_Connect.php";
		$database = new DB_Connect();

		$this->conn = $database->connect();
	}

	function __destruct() {
		// Do nothing
	}

	public function generate_id($id_kantor) {
		$year = substr(date("Y"), 2, 2);
		$last_number_id = $this->get_last_number_id($id_kantor, $year);
		$last_number = substr($last_number_id, 10, 6);
		$last_number = (int) $last_number;
		$last_number++;

		while (strlen($last_number) < 6) {
			$last_number = "0".$last_number;
		}

		if (strlen($id_kantor) == 5) {
			$id_delivery_order = $id_kantor."00".$year."A".$last_number;
		} elseif(strlen($id_kantor) == 6) {
			$id_delivery_order = $id_kantor."0".$year."A".$last_number;
		} elseif (strlen($id_kantor) == 7) {
			$id_delivery_order = $id_kantor.$year."A".$last_number;
		}

		return $id_delivery_order;
	}

	public function get_last_number_id($id_kantor, $year) {
		$length = strlen($id_kantor);
		if ($length == 5) {
			$id_kantor2 = $id_kantor."00";
		} elseif ($length == 6) {
			$id_kantor2 = $id_kantor."0";
		} elseif ($length == 7) {
			$id_kantor2 = $id_kantor;
		}

		$query = "
			SELECT TOP 1 id FROM delivery_order
			WHERE id LIKE '$id_kantor2$year%'
			ORDER BY id DESC
		";
		$stmt = mssql_query($query, $this->conn);
		$row = mssql_fetch_array($stmt);

		return $row["id"];
	}

	public function insert($id, $id_pengantar) {
		$date = date("Y-m-d");
		$query = "
			INSERT INTO delivery_order (id, id_pengantar, tanggal)
			VALUES ('$id', '$id_pengantar', '$date')
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_id($id_pengantar, $id_kantor) {
		$length = strlen($id_kantor);
		if ($length == 5) {
			$id_kantor2 = $id_kantor."00";
		} elseif ($length == 6) {
			$id_kantor2 = $id_kantor."0";
		} elseif ($length == 7) {
			$id_kantor2 = $id_kantor;
		}

		$query = "
			SELECT TOP 1 DO.id, P.id_kantor FROM delivery_order DO
			JOIN pengantar P ON DO.id_pengantar=P.id
			WHERE DO.id_pengantar='$id_pengantar' AND DO.id LIKE '$id_kantor2%' AND P.id_kantor='$id_kantor'
			ORDER BY DO.id DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$row = mssql_fetch_array($stmt);
			return $row["id"];
		} else {
			return false;
		}
	}

	public function is_empty($id_pengantar, $id_kantor) {
		$length = strlen($id_kantor);
		if ($length == 5) {
			$id_kantor2 = $id_kantor."00";
		} elseif ($length == 6) {
			$id_kantor2 = $id_kantor."0";
		} elseif ($length == 7) {
			$id_kantor2 = $id_kantor;
		}

		$query = "
			SELECT DO.*, P.id_kantor FROM delivery_order DO
			JOIN pengantar P ON DO.id_pengantar=P.id
			WHERE DO.id_pengantar='$id_pengantar' AND DO.id LIKE '$id_kantor2%' AND P.id_kantor='$id_kantor'
		";
		$stmt = mssql_query($query, $this->conn);

		if (mssql_num_rows($stmt) == 0) {
			return true;
		} else {
			return false;
		}
	}

	public function is_close($id_pengantar, $id_kantor) {
		$length = strlen($id_kantor);
		if ($length == 5) {
			$id_kantor2 = $id_kantor."00";
		} elseif ($length == 6) {
			$id_kantor2 = $id_kantor."0";
		} elseif ($length == 7) {
			$id_kantor2 = $id_kantor;
		}

		$query = "
			SELECT TOP 1 DO.tutup FROM delivery_order DO
			JOIN pengantar P ON DO.id_pengantar=P.id
			WHERE DO.id_pengantar='$id_pengantar' AND DO.id LIKE '$id_kantor2%' AND P.id_kantor='$id_kantor'
			ORDER BY DO.id DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$row = mssql_fetch_array($stmt);

			if ($row["tutup"] == '1') {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function get_tanggal($id_pengantar, $id_kantor) {
		require_once "My_Date.php";
		$my_date = new My_Date();

		$length = strlen($id_kantor);
		if ($length == 5) {
			$id_kantor2 = $id_kantor."00";
		} elseif ($length == 6) {
			$id_kantor2 = $id_kantor."0";
		} elseif ($length == 7) {
			$id_kantor2 = $id_kantor;
		}

		$query = "
			SELECT TOP 1 DO.tanggal FROM delivery_order DO
			JOIN pengantar P ON DO.id_pengantar=P.id
			WHERE DO.id_pengantar='$id_pengantar' AND DO.id LIKE '$id_kantor2%' AND P.id_kantor='$id_kantor'
			ORDER BY DO.id DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$row = mssql_fetch_array($stmt);
			return $my_date->convert_to_date($row["tanggal"]);
		} else {
			return date("Y-m-d");
		}
	}

	public function update($id_pengantar, $id_delivery_order, $jumlah_item) {
		$date = date("Y-m-d");
		$query = "
			UPDATE delivery_order SET jumlah_item='$jumlah_item', tutup='1' 
			WHERE id='$id_delivery_order' AND id_pengantar='$id_pengantar' AND tanggal='$date'
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			return true;
		} else {
			return false;
		}
	}
}
?>
