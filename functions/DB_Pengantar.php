<?php

class DB_Pengantar {
	private $conn;

	function __construct() {
		require_once "database/DB_Connect.php";
		$database = new DB_Connect();

		$this->conn = $database->connect();
	}

	function __destruct() {
		// Do nothing
	}

	/*
	 * Insert into table pengantar
	 */
	public function insert($id, $nama, $id_login, $password, $imei, $id_kantor, $id_grup, $id_mandor, $nama_mandor, $versi, $server) {
		$query = "
			INSERT INTO pengantar (id, nama, id_login, password, imei, id_kantor, id_grup, id_mandor, nama_mandor, versi, server)
			VALUES ('$id', '$nama', '$id_login', '$password', '$imei', '$id_kantor', '$id_grup', '$id_mandor', '$nama_mandor', '$versi', '$server')
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function update($id, $nama, $id_login, $password, $imei, $id_kantor, $id_grup, $id_mandor, $nama_mandor, $versi, $server) {
		$query = "
			UPDATE pengantar SET nama='$nama', id_login='$id_login', password='$password', imei='$imei', id_kantor='$id_kantor', id_grup='$id_grup',
				id_mandor='$id_mandor', nama_mandor='$nama_mandor', versi='$versi', server='$server'
			WHERE id='$id'
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function is_user_exist($id) {
		$query = "SELECT * FROM pengantar WHERE id='$id'";
		$stmt = mssql_query($query, $this->conn);

		if (mssql_num_rows($stmt) == 1) {
			return true;
		} else {
			return false;
		}
	}
}
?>
