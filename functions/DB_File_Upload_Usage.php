<?php

class DB_File_Upload_Usage {
    private $conn;

    function __construct() {
        require_once "database/DB_Connect.php";
        $database = new DB_Connect();

        $this->conn = $database->connect();
    }

    function __destruct() {
        // Do nothing
    }

    public function get_all($id_pengantar) {
        $query = "SELECT * FROM file_upload_usage WHERE id_pengantar='$id_pengantar' ORDER BY tanggal ASC";
        $stmt = mssql_query($query, $this->conn);

        if ($stmt) {
            if (mssql_num_rows($stmt) == 0) {
        		return "empty";
            } else {
                for($i = 0; $i <= (mssql_num_rows($stmt) - 1); $i++) {
        			$row[] = mssql_fetch_array($stmt);
        		}

        		return $row;
            }
        } else {
            return false;
        }
    }

    public function is_empty($tanggal, $id_pengantar) {
        $query = "SELECT * FROM file_upload_usage WHERE tanggal='$tanggal' AND id_pengantar='$id_pengantar'";
        $stmt = mssql_query($query, $this->conn);

        if (mssql_num_rows($stmt) == 0) {
            return true;
        } else {
            return false;
        }
    }

    public function get_latest_data($tanggal, $id_pengantar) {
        $query = "SELECT * FROM file_upload_usage WHERE tanggal='$tanggal' AND id_pengantar='$id_pengantar'";
        $stmt = mssql_query($query, $this->conn);

        if ($stmt > 0) {
            $row = mssql_fetch_array($stmt);
            return $row;
        }
    }

    public function insert(
        $uid, $id_pengantar, $tanggal, $jml_berkas, $jml_berkas_foto, $jml_berkas_ttd, $jml_ukuran,
        $jml_ukuran_foto, $jml_ukuran_ttd
    ) {
        $query = "
            INSERT INTO file_upload_usage (uid, id_pengantar, tanggal, jml_berkas, jml_berkas_foto, jml_berkas_ttd, jml_ukuran,
                jml_ukuran_foto, jml_ukuran_ttd)
            VALUES ('$uid', '$id_pengantar', '$tanggal', '$jml_berkas', '$jml_berkas_foto', '$jml_berkas_ttd',
                '$jml_ukuran', '$jml_ukuran_foto', '$jml_ukuran_ttd')
        ";
        $stmt = mssql_query($query, $this->conn);

        if ($stmt > 0) {
            $query2 = "SELECT * FROM file_upload_usage WHERE tanggal='$tanggal' AND id_pengantar='$id_pengantar'";
            $stmt2 = mssql_query($query2, $this->conn);

            if ($stmt2) {
                $row = mssql_fetch_array($stmt2);
                return $row;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function update(
        $uid, $jml_berkas, $jml_berkas_foto, $jml_berkas_ttd, $jml_ukuran, $jml_ukuran_foto, $jml_ukuran_ttd
    ) {
        $query = "
            UPDATE file_upload_usage SET jml_berkas='$jml_berkas', jml_berkas_foto='$jml_berkas_foto', 
                jml_berkas_ttd='$jml_berkas_ttd', jml_ukuran='$jml_ukuran', jml_ukuran_foto='$jml_ukuran_foto', 
                jml_ukuran_ttd='$jml_ukuran_ttd'
            WHERE uid='$uid'
        ";
        $stmt = mssql_query($query, $this->conn);

        if ($stmt > 0) {
            return true;
        } else {
            return false;
        }
    }
}


?>
