<?php

class DB_Versi {

	private $conn;

	function __construct() {
		require_once "database/DB_Connect.php";
		$database = new DB_Connect();

		$this->conn = $database->connect();
	}

	function __destruct() {
		// Do nothing
	}

	public function get_keterangan($type) {
		$query = "
			SELECT TOP 1 keterangan FROM versi 
			WHERE tipe='$type'
			ORDER BY id DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$row = mssql_fetch_array($stmt);
			return $row["keterangan"];
		}
	}

	public function get_tipe_update($type) {
		$query = "
			SELECT TOP 1 tipe_update FROM versi 
			WHERE tipe='$type'
			ORDER BY id DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$row = mssql_fetch_array($stmt);
			return $row["tipe_update"];
		}
	}

	public function get_id($type) {
		$query = "
			SELECT TOP 1 id FROM versi 
			WHERE tipe='$type'
			ORDER BY id DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$row = mssql_fetch_array($stmt);
			return $row["id"];
		}
	}

	public function is_latest_version($device_version, $type) {
		$query = "
			SELECT TOP 1 id FROM versi 
		  	WHERE tipe='$type'
	  		ORDER BY id DESC
  		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$row = mssql_fetch_array($stmt);

			if ($device_version == $row["id"]) {
				return true;
			} else {
				return false;
			}
		}
	}
}

?>
