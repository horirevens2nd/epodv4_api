<?php

class DB_Antaran {
	private $conn;

	function __construct() {
		require_once "database/DB_Connect.php";
		$database = new DB_Connect();

		$this->conn = $database->connect();
	}

	function __destruct() {
		// Do nothing
	}

	public function is_id_item_exist($id_item) {
		$query = "SELECT * FROM antaran WHERE id_item='$id_item'";
		$stmt = mssql_query($query, $this->conn);

		if (mssql_num_rows($stmt) == 0) {
			return false;
		} else {
			return true;
		}
	}

	public function get_nama_pengantar($id_item) {
		$query = "
			SELECT TOP 1 P.nama FROM antaran A
			JOIN delivery_order D ON A.id_delivery_order=D.id
			JOIN pengantar P ON D.id_pengantar=P.id
			WHERE A.id_item='$id_item'
			ORDER BY waktu_entri DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt) {
			$row = mssql_fetch_array($stmt);
			return $row["nama"];
		}
	}

	public function get_id_status($id_item) {
		$query = "
			SELECT TOP 1 id_status FROM antaran
			WHERE id_item='$id_item'
			ORDER BY waktu_entri DESC
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt) {
			$row = mssql_fetch_array($stmt);
			return $row["id_status"];
		}
	}

	public function is_less_than_three_times($id_item) {
		$query = "
			SELECT COUNT(A.id_item) FROM antaran A
			JOIN delivery_order DO ON A.id_delivery_order=DO.id
			WHERE A.id_item='$id_item'
			GROUP BY DO.tanggal
		";
		$stmt = mssql_query($query, $this->conn);

		if (mssql_num_rows($stmt) < 3) {
			return true;
		} else {
			return false;
		}
	}

	public function insert(
		$id_item, $id_delivery_order, $waktu_entri, $id_mandor, $id_pengantar, $id_kantor, $nama_pengirim, $alamat_pengirim, $hp_pengirim, $nama_penerima, $alamat_penerima, $hp_penerima, $id_produk, $berat, $bsu_cod, $bsu_blb, $bsu_tax, $kantor_asal, $kantor_tujuan, $id_customer, $id_external, $tgl_posting, $tipe
	) {
		$uid = uniqid("", true);
		$query = "
			INSERT INTO antaran (uid, id_item, id_delivery_order, id_status, waktu_entri, nama_pengirim, alamat_pengirim, hp_pengirim, nama_penerima, alamat_penerima, hp_penerima, id_produk, berat, bsu_cod, bsu_blb, bsu_tax, kantor_asal, kantor_tujuan, id_customer, id_external, tgl_posting, tipe)
			VALUES ('$uid', '$id_item', '$id_delivery_order', 'P01', '$waktu_entri', '$nama_pengirim', '$alamat_pengirim', '$hp_pengirim', '$nama_penerima', '$alamat_penerima', '$hp_penerima', '$id_produk', '$berat', '$bsu_cod', '$bsu_blb', '$bsu_tax', '$kantor_asal', '$kantor_tujuan', '$id_customer','$id_external', '$tgl_posting', '$tipe')
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$uid2 = uniqid("", true);
			$query2 = "
				INSERT INTO PRA_ANTAR (uid, id_antaran, KDITEM, KDANTARAN, NIPPOSMANDOR, NIPPOSANTAR, KDNOPEN, WKTLOKAL)
				VALUES ('$uid2', '$uid', '$id_item', '$id_delivery_order', '$id_mandor', '$id_pengantar', '$id_kantor', '$waktu_entri')
			";
			$stmt2 = mssql_query($query2, $this->conn);

			if ($stmt2 > 0) {
				$query3 = "SELECT * FROM antaran WHERE id_item='$id_item' AND id_delivery_order='$id_delivery_order'";
				$stmt3 = mssql_query($query3, $this->conn);
				$row = mssql_fetch_array($stmt3);

				return $row;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function delete_by_uid($uid) {
		$query = "DELETE FROM antaran WHERE uid='$uid'";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function delete_by_id_delivery_order($id_delivery_order) {
		$query = "DELETE FROM antaran WHERE id_delivery_order='$id_delivery_order'";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function get_all($id_pengantar) {
		$date = date("Y-m-d");
		$query = "
			SELECT A.*, DO.tutup FROM antaran A
			JOIN delivery_order DO ON A.id_delivery_order=DO.id
			WHERE DO.id_pengantar='$id_pengantar' AND DO.tanggal='$date'
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			if (mssql_num_rows($stmt) == 0) {
				return "empty";
			} else {
				for($i = 0; $i <= (mssql_num_rows($stmt) - 1); $i++) {
					$row[] = mssql_fetch_array($stmt);
				}

				return $row;
			}
		} else {
			return false;
		}
	}

	/* public function get_all_with_detail($id_pengantar) {
		$date = date("Y-m-d");
		$query = "
			SELECT A.*, DO.tutup, DA.id_produk, DA.id_penerima, DA.al_penerima, DA.id_pengirim, DA.swp FROM antaran A
			JOIN delivery_order DO ON A.id_delivery_order=DO.id
			LEFT JOIN detail_alamat DA ON A.id_item=DA.id_item
			WHERE DO.id_pengantar='$id_pengantar' AND DO.tanggal='$date';
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			if (mssql_num_rows($stmt) == 0) {
				return "empty";
			} else {
				for($i = 0; $i <= (mssql_num_rows($stmt) - 1); $i++) {
					$row[] = mssql_fetch_array($stmt);
				}

				return $row;
			}
		} else {
			return false;
		}
	} */

	public function update(
		$uid, $id_status, $keterangan, $waktu_update, $garis_lintang, $garis_bujur, $tanda_tangan, $foto, $id_item, $id_delivery_order, $id_pengantar,
		$id_kantor, $kode_dn, $alamat_baru, $catatan
	) {
		$query = "
			UPDATE antaran SET id_status='$id_status', keterangan='$keterangan', waktu_update='$waktu_update', garis_lintang='$garis_lintang',
				garis_bujur='$garis_bujur', tanda_tangan='$tanda_tangan', foto='$foto', alamat_baru='$alamat_baru', catatan='$catatan'
			WHERE uid='$uid'
		";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			$uid2 = uniqid("", true);
			$query2 = "
				INSERT INTO PASKA_ANTAR (uid, id_antaran, KDITEM, KDANTARAN, NIPPOSANTAR, NIPPOSUPDATE, KDNOPEN, KDSTATUS, KETERANGAN,
					WKTANTAR, WKTUPDATE)
				VALUES ('$uid2', '$uid', '$id_item', '$id_delivery_order', '$id_pengantar', '$id_pengantar', '$id_kantor', '$kode_dn', '$keterangan', '$waktu_update',
					'$waktu_update')
			";
			$stmt2 = mssql_query($query2, $this->conn);

			if ($stmt2 > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
}

?>
