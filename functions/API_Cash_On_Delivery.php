<?php

class API_Cash_On_Delivery {
    private $user = "epod";
    private $password = "3?0d!w4xw4W";
    private $consumer_key = "kxWno14XJgc7hOt0VOoDUmMKtPMa";
    private $consumer_secret = "LzRYL_ElYuPjt_tk7d6bjFroS60a";

    public function get_token() {
        $url = "https://api.posindonesia.co.id:8245/token";
        $header = [
            "Content-Type: application/x-www-form-urlencoded",
            "Authorization: Basic " . base64_encode($this->consumer_key.":".$this->consumer_secret)
        ];
        $body = "grant_type=client_credentials";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $response = curl_exec($ch);
        if (curl_errno($ch)) {
            return false;
            curl_close($ch);
        } else {
            $json_object = json_decode($response, true);
            return $json_object['access_token'];
            curl_close($ch);
        }
    }

    public function get_item($barcode) {
        $url = "https://api.posindonesia.co.id:8245/epod/1.0.0/getItem";
        $header = [
            "Accept: application/json; charset=utf-8",
            "Content-Type: application/json; charset=utf-8",
            "X-POS-USER: " . $this->user,
            "X-POS-PASSWORD: " . $this->password,
            "Authorization: Bearer " . $this->get_token()
        ];
        $body = ["barcode" => $barcode];
        $body = json_encode($body);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $response = curl_exec($ch);
        curl_close($ch);

        $json_object = json_decode($response, true);
        return $json_object;
    }

    /*public function get_cod_item_status($barcode) {
        $url = "https://api.posindonesia.co.id:8245/epod/1.0.0/getCodItemStatus";
        $header = [
            "Accept: application/json; charset=utf-8",
            "Content-Type: application/json; charset=utf-8",
            "X-POS-USER: " . $this->user,
            "X-POS-PASSWORD: " . $this->password,
            "Authorization: Bearer " . $this->get_token()
        ];
        $body = ["barcode" => $barcode];
        $body = json_encode($body);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $response = curl_exec($ch);
        curl_close($ch);

        $json_object = json_decode($response, true);
        return $json_object;
    }

    public function get_blb_item_status($barcode) {
        $url = "https://api.posindonesia.co.id:8245/epod/1.0.0/getBlbItemStatus";
        $header = [
            "Accept: application/json; charset=utf-8",
            "Content-Type: application/json; charset=utf-8",
            "X-POS-USER: " . $this->user,
            "X-POS-PASSWORD: " . $this->password,
            "Authorization: Bearer " . $this->get_token()
        ];
        $body = ["barcode" => $barcode];
        $body = json_encode($body);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        $response = curl_exec($ch);
        curl_close($ch);

        $json_object = json_decode($response, true);
        return $json_object;
    }*/
}



?>
