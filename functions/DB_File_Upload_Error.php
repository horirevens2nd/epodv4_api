<?php

class DB_File_Upload_Error {
	private $conn;
	
	function __construct() {
		require_once "database/DB_Connect.php";
		$database = new DB_Connect();

		$this->conn = $database->connect();
	}

	function __destruct() {
		// Do nothing
	}

	public function is_nama_file_exist($nama_file) {
		$query = "SELECT * FROM file_upload_error WHERE nama_file='$nama_file'";
		$stmt = mssql_query($query, $this->conn);

		if (mssql_num_rows($stmt) > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function insert($uid, $id_pengantar, $nama_file, $direktori) {
		$query = "SELECT * FROM file_upload_error WHERE nama_file='$nama_file'";
		$stmt = mssql_query($query, $this->conn);

		if (mssql_num_rows($stmt) == 0) {
			$query2 = "
				INSERT INTO file_upload_error (uid, id_pengantar, nama_file, direktori)
				VALUES ('$uid', '$id_pengantar', '$nama_file', '$direktori')
			";
			$stmt2 = mssql_query($query2, $this->conn);

			if ($stmt2 > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function update($uid, $id_pengantar, $nama_file) {
		$query = "UPDATE file_upload_error SET uid='$uid' WHERE id_pengantar='$id_pengantar' AND nama_file='$nama_file'";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			return true;
		} else {
			return false;
		}
	}

	public function delete($id_pengantar, $nama_file) {
		$query = "SELECT * FROM file_upload_error WHERE id_pengantar='$id_pengantar' AND nama_file='$nama_file'";
		$stmt = mssql_query($query, $this->conn);

		if (mssql_num_rows($stmt) > 0) {
			$query2 = "DELETE FROM file_upload_error WHERE id_pengantar='$id_pengantar' AND nama_file='$nama_file'";
			$stmt2 = mssql_query($query2, $this->conn);

			if ($stmt2 > 0) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}		
	}

	public function get_all($id_pengantar) {
		$query = "SELECT * FROM file_upload_error WHERE id_pengantar='$id_pengantar'";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt) {
            if (mssql_num_rows($stmt) == 0) {
        		return "empty";
            } else {
                for($i = 0; $i <= (mssql_num_rows($stmt) - 1); $i++) {
        			$row[] = mssql_fetch_array($stmt);
        		}

        		return $row;
            }
        } else {
            return false;
        }
	}
}


?>
