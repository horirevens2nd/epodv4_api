<?php

class DB_Status {
	private $conn;

	function __construct() {
		require_once "database/DB_Connect.php";
		$database = new DB_Connect();

		$this->conn = $database->connect();
	}

	function __destruct() {
		// Do nothing
	}

	function get_all() {
		$query = "SELECT * FROM status";
		$stmt = mssql_query($query, $this->conn);

		if ($stmt > 0) {
			if (mssql_num_rows($stmt) == 0) {
				return "empty";
			} else {
				for($i = 0; $i <= (mssql_num_rows($stmt) - 1); $i++) {
					$row[] = mssql_fetch_array($stmt);
				}

				return $row;
			}
		} else {
			mssql_free_result($stmt);
			return false;
		}
	}
}

?>
