<?php

class My_Date {

	public function get_month_as_number($month) {
		switch ($month) {
			case "Jan":
				$result = "01";
				break;
			case "Feb":
				$result = "02";
				break;
			case "Mar":
				$result = "03";
				break;
			case "Apr":
				$result = "04";
				break;
			case "May":
				$result = "05";
				break;
			case "Jun":
				$result = "06";
				break;
			case "Jul":
				$result = "07";
				break;
			case "Aug":
				$result = "08";
				break;
			case "Sep":
				$result = "09";
				break;
			case "Oct":
				$result = "10";
				break;
			case "Nov":
				$result = "11";
				break;
			case "Dec":
				$result = "12";
				break;
		}

		return $result;
	}

	public function convert_to_date_indo($date) {
		$year = substr($date, 0, 4);
		$month = substr($date, 5, 2);
		$day = substr($date, 8, 2);
		$result = $day."-".$month."-".$year;

		return $result;
	}

	public function convert_to_date($long_date) { // Apr 19 2018 12:00:00:AM
		$mDay = trim(substr($long_date, 4, 2));
		if (strlen($mDay) == 1) {
			$day = "0".$mDay;
		} else {
			$day = $mDay;
		}

		$month = $this->get_month_as_number(substr($long_date, 0, 3));
		$year = substr($long_date, 7, 4);
		$date = $year."-".$month."-".$day;

		return $date;
	}

	public function convert_to_24_hour($hour, $type) {
		switch ($hour) {
			case "01":
				if ($type == "PM") {
					$result = "13";
				} else {
					$result = "01";
				}
				break;
			case "02":
				if ($type == "PM") {
					$result = "14";
				} else {
					$result = "02";
				}
				break;
			case "03":
				if ($type == "PM") {
					$result = "15";
				} else {
					$result = "03";
				}
				break;
			case "04":
				if ($type == "PM") {
					$result = "16";
				} else {
					$result = "04";
				}
				break;
			case "05":
				if ($type == "PM") {
					$result = "17";
				} else {
					$result = "05";
				}
				break;
			case "06":
				if ($type == "PM") {
					$result = "18";
				} else {
					$result = "06";
				}
				break;
			case "07":
				if ($type == "PM") {
					$result = "19";
				} else {
					$result = "07";
				}
				break;
			case "08":
				if ($type == "PM") {
					$result = "20";
				} else {
					$result = "08";
				}
				break;
			case "09":
				if ($type == "PM") {
					$result = "21";
				} else {
					$result = "09";
				}
				break;
			case "10":
				if ($type == "PM") {
					$result = "22";
				} else {
					$result = "10";
				}
				break;
			case "11":
				if ($type == "PM") {
					$result = "23";
				} else {
					$result = "11";
				}
				break;
			case "12":
				if ($type == "PM") {
					$result = "12";
				} else {
					$result = "00";
				}
				break;
		}

		return $result;
	}

	public function convert_to_datetime($long_date) { // Apr 19 2018 12:00:00:AM
		$mDay = trim(substr($long_date, 4, 2));
		if (strlen($mDay) == 1) {
			$day = "0".$mDay;
		} else {
			$day = $mDay;
		}

		$month = $this->get_month_as_number(substr($long_date, 0, 3));
		$year = substr($long_date, 7, 4);
		$hour = $this->convert_to_24_hour(substr($long_date, 12, 2), substr($long_date, 21, 2));
		$minute = substr($long_date, 15, 2);
		$second = substr($long_date, 18, 2);
		$datetime = $year."-".$month."-".$day." ".$hour.":".$minute.":".$second;

		return $datetime;
	}
}
?>
