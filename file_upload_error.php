<?php

require_once "functions/DB_File_Upload_Error.php";
$database = new DB_File_Upload_Error();

$response = array("error" => false);

if (isset($_POST["action"]) && $_POST["action"] == "submit") {
	$uid = $_POST["uid"];
	$id_pengantar = $_POST["id_pengantar"];
	$nama_file = $_POST["nama_file"];
	$direktori = $_POST["direktori"];

	if ($database->is_nama_file_exist($nama_file)) {
		if ($database->update($uid, $id_pengantar, $nama_file)) {
			$response["error"] = false;
			$response["file_upload_error"]["action"] = "update";
			$response["file_upload_error"]["uid"] = $uid;
			$response["file_upload_error"]["id_pengantar"] = $id_pengantar;
			$response["file_upload_error"]["nama_file"] = $nama_file;
			$response["file_upload_error"]["direktori"] = $direktori;
			echo json_encode($response);
		} else {
			$response["error"] = true;
			$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
			echo json_encode($response);
		}
	} else {
		if ($database->insert($uid, $id_pengantar, $nama_file, $direktori)) {
			$response["error"] = false;
			$response["file_upload_error"]["action"] = "insert";
			$response["file_upload_error"]["uid"] = $uid;
			$response["file_upload_error"]["id_pengantar"] = $id_pengantar;
			$response["file_upload_error"]["nama_file"] = $nama_file;
			$response["file_upload_error"]["direktori"] = $direktori;
			echo json_encode($response);
		} else {
			$response["error"] = true;
			$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
			echo json_encode($response);
		}
	}
}

if (isset($_POST["action"]) && $_POST["action"] == "delete") {
	$id_pengantar = $_POST["id_pengantar"];
	$nama_file = $_POST["nama_file"];
	
	if ($database->delete($id_pengantar, $nama_file)) {
		$response["error"] = false;
		echo json_encode($response);
	} else {
		$response["error"] = true;
		echo json_encode($response);
	}
}

if (isset($_POST["action"]) && $_POST["action"] == "get_all") {
	$id_pengantar = $_POST["id_pengantar"];
	$result = $database->get_all($id_pengantar);

	if ($result == "empty") {
	    $response["error"] = false;
	    $response["message"] = "empty";
	    echo json_encode($response);
  	} else if ($result == false) {
	    $response["error"] = true;
	  	$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
	  	echo json_encode($response);
  	} else {
	    $length = count($result);
	    $response["error"] = false;
	    $response["message"] = "not_empty";

	    for ($i=0; $i<$length; $i++) {
			$response["file_upload_error"][$i]["uid"] = $result[$i]["uid"];
			$response["file_upload_error"][$i]["id_pengantar"] = $result[$i]["id_pengantar"];
			$response["file_upload_error"][$i]["nama_file"] = $result[$i]["nama_file"];
			$response["file_upload_error"][$i]["direktori"] = $result[$i]["direktori"];
    	}
    	echo json_encode($response);
  	}
}

?>
