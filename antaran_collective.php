<?php
//header("Content-Type: application/json");
require_once "functions/DB_Antaran.php";
$database = new DB_Antaran();
require_once "functions/My_Date.php";
$my_date = new My_Date();

$json = file_get_contents("php://input");
$object = json_decode($json, true);
$action = $object["action"]; //$object->action;
$data = $object["data"]; //$object->data;

if ($action == "update_collective") {
	$i = 0;
	$j = 0;

	foreach ($data as $value) {
		$uid = $value["uid"];
		$id_item = $value["id_item"];
		$id_delivery_order = $value["id_delivery_order"];
		$id_status = $value["id_status"];
		$keterangan = strtoupper($value["keterangan"]);
		$waktu_update = $value["waktu_update"];
		$garis_lintang = $value["garis_lintang"];
		$garis_bujur = $value["garis_bujur"];
		$tanda_tangan = $value["tanda_tangan"];
		$foto = $value["foto"];
		$id_pengantar = $value["id_pengantar"];
		$id_kantor = $value["id_kantor"];
		$kode_dn = $value["kode_dn"];
		$tanggal_perangkat = $value["tanggal_perangkat"];
		$tanggal_server = date("Y-m-d");
        $alamat_baru = $value["alamat_baru"];
        $catatan = $value["catatan"];

		if ($tanggal_server == $tanggal_perangkat) {
				if ($database->update(
					$uid, $id_status, $keterangan, $waktu_update, $garis_lintang, $garis_bujur, $tanda_tangan, $foto, $id_item,
					$id_delivery_order, $id_pengantar, $id_kantor, $kode_dn, $alamat_baru, $catatan)
				) {
					$response["antaran"][$j]["uid"] = $uid;
					$response["antaran"][$j]["tanda_tangan"] = $tanda_tangan;
					$response["antaran"][$j]["foto"] = $foto;
					$i++;
				}
				$j++;

				if (count($data) == $j) {
					if ($i== 0) {
						$response["error"] = true;
						$response["error_message"] = "Update status kolektif gagal. Silakan dicoba kembali";
						echo json_encode($response);
					} else {
						$response["error"] = false;
						$response["message"] = "Update status kolektif berhasil dengan jumlah " . $i . " item";
						echo json_encode($response);
					}
				}
		} else {
			$response["error"] = true;
			$response["error_type"] = "alert";
			$response["error_message"] =
				"Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server
				\nTanggal Perangkat : " . $my_date->convert_to_date_indo($tanggal_perangkat) . "\nTanggal Server       : " . $my_date->convert_to_date_indo($tanggal_server);
			echo json_encode($response);
		}
	}
}

?>
