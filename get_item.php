<?php
header('Content-Type: application/json');

//require_once "functions/DB_Pengantar.php";
//$database = new DB_Pengantar();
require_once "functions/API_Cash_On_Delivery.php";
$api_cod = new API_Cash_On_Delivery();

$id_item = isset($_GET["id_item"]) ? $_GET["id_item"] : "";

$get_item = $api_cod->get_item($id_item);
echo json_encode($get_item);
?>
