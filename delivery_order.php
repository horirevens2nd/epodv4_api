<?php

require_once "functions/DB_Delivery_Order.php";
$database = new DB_Delivery_Order();
require_once "functions/My_Date.php";
$my_date = new My_Date();

$response = array("error" => false);

if (isset($_POST["action"]) && $_POST["action"] == "generate_id") {
	$id_pengantar = $_POST["id_pengantar"];
	$id_kantor = $_POST["id_kantor"];
	$tanggal_perangkat = $_POST["tanggal_perangkat"];
	$tanggal_server = date("Y-m-d");

	if ($tanggal_server != $tanggal_perangkat) {
		$response["error"] = true;
		$response["error_message"] =
			"Tanggal pada perangkat Anda tidak sesuai dengan tanggal pada server
			\nTanggal Perangkat : " . $my_date->convert_to_date_indo($tanggal_perangkat) . "\nTanggal Server       : " . $my_date->convert_to_date_indo($tanggal_server);
		echo json_encode($response);
	} else {
		if ($database->is_empty($id_pengantar, $id_kantor)) {
			$id = $database->generate_id($id_kantor);

			if ($database->insert($id, $id_pengantar)) {
				$response["error"] = false;
				$response["delivery_order"]["id"] = $id;
				echo json_encode($response);
			} else {
				$response["error"] = true;
				$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
				echo json_encode($response);
			}
		} else {
			if ($database->is_close($id_pengantar, $id_kantor)) {
				$id = $database->generate_id($id_kantor);

				if ($database->insert($id, $id_pengantar)) {
					$response["error"] = false;
					$response["delivery_order"]["id"] = $id;
					echo json_encode($response);
				} else {
					$response["error"] = true;
					$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
					echo json_encode($response);
				}
			} else {
				if ($tanggal_server == $database->get_tanggal($id_pengantar, $id_kantor)) {
					if ($database->get_id($id_pengantar, $id_kantor)) {
						$response["error"] = false;
						$response["delivery_order"]["id"] = $database->get_id($id_pengantar, $id_kantor);
						echo json_encode($response);
					} else {
						$response["error"] = true;
						$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
						echo json_encode($response);
					}
				} else {
					$id = $database->generate_id($id_kantor);

					if ($database->insert($id, $id_pengantar)) {
						$response["error"] = false;
						$response["delivery_order"]["id"] = $id;
						echo json_encode($response);
					} else {
						$response["error"] = true;
						$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
						echo json_encode($response);
					}
				}
			}
		}
	}
}

if (isset($_POST["action"]) && $_POST["action"] == "close") {
	$id_pengantar = $_POST["id_pengantar"];
	$id_delivery_order = $_POST["id_delivery_order"];
	$jumlah_item = $_POST["jumlah_item"];

	if ($database->update($id_pengantar, $id_delivery_order, $jumlah_item)) {
		$response["error"] = false;
		echo json_encode($response);
	} else {
		$response["error"] = true;
		$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
		echo json_encode($response);
	}
}

?>
