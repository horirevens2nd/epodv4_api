<?php

require_once "functions/DB_Status.php";
$database = new DB_Status();

$response = array("error" => false);

if (isset($_POST["action"]) && $_POST["action"] == "get_all") {
	$result = $database->get_all();

	if ($result == "empty") {
		$response["error"] = false;
		$response["message"] = "empty";
		echo json_encode($response);
	} else if ($result == false) {
		$response["error"] = true;
		$response["error_message"] = "Terjadi kesalahan saat menjalankan perintah. Silakan dicoba kembali";
		echo json_encode($response);
	} else {
		$length = count($result);
		$response["error"] = false;
		$response["message"] = "not_empty";

		for ($i=0; $i<$length; $i++) {
			$response["status"][$i]["id"] = $result[$i]["id"];
			$response["status"][$i]["keterangan"] = $result[$i]["keterangan"];
			$response["status"][$i]["tindakan"] = $result[$i]["tindakan"];
			$response["status"][$i]["kode_ln"] = $result[$i]["kode_ln"];
			$response["status"][$i]["kode_dn"] = $result[$i]["kode_dn"];
			$response["status"][$i]["urutan"] = $result[$i]["urutan"];
			$response["status"][$i]["level"] = $result[$i]["level"];
			$response["status"][$i]["terakhir"] = $result[$i]["terakhir"];
			$response["status"][$i]["id_status_induk"] = $result[$i]["id_status_induk"];
		}
		echo json_encode($response);
	}
}

?>
