<?php

require_once "functions/DB_Versi.php";
$database = new DB_Versi();

if (isset($_POST["versi"]) AND isset($_POST["tipe"])) {
	$versi = $_POST["versi"];
	$tipe = $_POST["tipe"];

	if ($database->is_latest_version($versi, $tipe)) {
		$response["error"] = false;
		echo json_encode($response);
	} else {
		$response["error"] = true;
		$response["error_message"] = $database->get_keterangan($tipe);
		$response["latest_version"] = $database->get_id($tipe);
		$response["type_version"] = $database->get_tipe_update($tipe);
		echo json_encode($response);
	}
}

?>
